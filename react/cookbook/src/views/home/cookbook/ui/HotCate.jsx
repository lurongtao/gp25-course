import React, { useState, useEffect } from 'react'
import Grid from '../../../../components/grid/Grid'
// import { HeartFill } from 'antd-mobile-icons'
import { HotCateWrapper, HotCateTitle } from './styledCookbook'
import axios from 'axios'

export default function Hotcate() {
  const [list, setList] = useState([])

  useEffect(() => {
    axios.get('http://localhost:9000/api/hotcate')
      .then((result) => {
        setList(result.data)
      })
      .catch((e) => {
        console.log(e)
      })
  }, [])

  return (
    <HotCateWrapper>
      <HotCateTitle
        borderWidth="0 0 1px 0"
      >热门分类</HotCateTitle>
      <div className="grid-wrap">
        <Grid
          columns={3}
          gap={10}
        >
          {
            list.map((li, index) => {
              return (
                <Grid.Item key={li.title}>
                  <div className="item-wrap">
                    <div>
                      <img src={li.img} alt="" />
                    </div>
                    <div>
                      {li.title}
                    </div>
                  </div>
                </Grid.Item>
              )
            })
          }
          <Grid.Item>
            <div className="item-wrap">
              <div>
                
              </div>
              <div>
                更多...
              </div>
            </div>
          </Grid.Item>
        </Grid>
      </div>
    </HotCateWrapper>
  )
}
