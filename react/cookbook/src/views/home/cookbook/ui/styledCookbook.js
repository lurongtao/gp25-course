import styled from 'styled-components'
import border from '../../../../components/styled/border'

const CookbookWrapper = styled.div `
  height: 100%;
  overflow-y: scroll;
  padding-bottom: .44rem;
  header {
    height: .44rem;
    background-color: #ee742f;
    line-height: .44rem;
    text-align: center;
    font-size: .18rem;
    color: #fff;
  }

  .swiper {
    height: 0;
    padding-bottom: 66.6666667%;
    img {
      width: 100%;
    }
  }
`

const HotCateWrapper = styled.div `
  background-color: #fff;

  .item-wrap {
    > div:first-child {
      text-align: center;
    }
    > div:last-child {
      text-align: center;
    }
    img {
      width: .6rem;
      height: .6rem;
      border-radius: .1rem;
    }
  }

  .grid-wrap {
    padding-top: .1rem;
  }
`

const HotCateTitle = border(
  styled.h3 `
  height: .44rem;
  line-height: .44rem;
  padding-left: .1rem;
`
)

const Top10Wrapper = styled.div `
  .item-wrap {
    img {
      width: 100%;
    }

    > div:last-child {
      display: flex;
      flex-direction: column;
      background-color: #fff;
      height: .6rem;
      align-items: center;
      justify-content: center;

      span:first-child {
        font-size: .2rem;
        font-weight: bold;
      }
    }
  }
`

export {
  CookbookWrapper,
  HotCateWrapper,
  HotCateTitle,
  Top10Wrapper
}