import React from 'react'

import {
  CookbookWrapper
} from './styledCookbook'

import Swiper from './Swiper'
import HotCate from './HotCate'
import Top10 from './Top10'
import Search from '../../../../components/search/Search'

export default function CookbookUi() {
  return (
    <CookbookWrapper>
      <header>美食大全</header>
      <div className="swiper">
        <Swiper></Swiper>
      </div>
      <Search
        outerBg="#eee"
        innerBg="#fff"
        hasBorder={true}
        borderRadius=".1rem"
      ></Search>
      <HotCate></HotCate>
      <Top10></Top10>
    </CookbookWrapper>
  )
}
