import React from 'react'
import Grid from '../../../../components/grid/Grid'
// import useLoadData from '../../../hooks/useLoadData'
import { HotCateTitle, Top10Wrapper } from './styledCookbook'
import { useSelector } from 'react-redux'

export default function Top10() {
  // const { list } = useLoadData()

  const list = useSelector(state => state.getIn(['cookbook', 'list']))

  return (
    <Top10Wrapper>
      <HotCateTitle
        borderWidth="0 0 1px 0"
      >精品好菜</HotCateTitle>
      <div className="grid-wrap">
        <Grid
          columns={2}
          gap={10}
        >
          {
            list.slice(0, 10).map((li) => (
              <Grid.Item key={li.id}>
                <div className="item-wrap">
                  <div>
                    <img src={li.img} alt="" />
                  </div>
                  <div>
                    <span>{li.name}</span>
                    <span>{li.all_click}浏览 {li.favorites}收藏</span>
                  </div>
                </div>
              </Grid.Item>
            ))
          }
        </Grid>
      </div>
    </Top10Wrapper>
  )
}
