import React from 'react'
import { Swiper } from 'antd-mobile'
import { useSelector } from 'react-redux'

export default function MySwiper() {
  const list = useSelector(state => state.getIn(['cookbook', 'list']))
  return (
    list.size > 0 && (
      <Swiper
        autoplay={false}
        allowTouchMove={true}
      >
        {
          list.slice(0, 5).map(li => (
            <Swiper.Item key={li.id}>
              <img src={li.img} alt="" />
            </Swiper.Item>
          ))
        }
      </Swiper>
    )
  )
}
