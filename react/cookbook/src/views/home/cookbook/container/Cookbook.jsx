import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import Ui from '../ui/CookbookUi'
import { LOADDATASAGA } from '../actionTypes'

export default function Cookbook() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch({
      type: LOADDATASAGA
    })
  }, [dispatch])

  return (
    <Ui></Ui>
  )
}
