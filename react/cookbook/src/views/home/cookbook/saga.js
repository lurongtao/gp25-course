import { takeEvery, put, call } from 'redux-saga/effects'
import axios from 'axios'
import { LOADDATA, LOADDATASAGA } from './actionTypes'

function* loadDataAsync() {
  // let result = yield axios.get('/api/list')
  let result = yield call(axios.get, '/api/list')

  // 相当于dispatch
  yield put({
    type: LOADDATA,
    list: result.data.data
  })
}

function* saga() {
  yield takeEvery(LOADDATASAGA, loadDataAsync)
}

export default saga