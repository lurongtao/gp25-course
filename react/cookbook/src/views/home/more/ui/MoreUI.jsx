import React from 'react'
import { Switch } from 'antd-mobile'

export default function MoreUI( { changeShow } ) {
  return (
    <div style={{padding: '.2rem'}}>
      显示地图：
      <Switch
        defaultChecked={true}
        onChange={changeShow}
      />
    </div>
  )
}
