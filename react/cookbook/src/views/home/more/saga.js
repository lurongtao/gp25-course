import { takeEvery, put } from 'redux-saga/effects'

import { CHANGESHOWSAGA } from './actionTypes'
import { changeShow } from './actionCreator'

function* saga() {
  yield takeEvery(CHANGESHOWSAGA, function* (action) {
    yield put(changeShow(action.show))
  })
}

export default saga