import React, { useCallback } from 'react'
import MoreUI from '../ui/MoreUI'
import { useDispatch } from 'react-redux'
import { changeShowSaga } from '../actionCreator'

export default function More() {
  const dispatch = useDispatch()

  const changeShow = useCallback((checked) => {
    dispatch(changeShowSaga(checked))
  }, [dispatch])

  return (
    <MoreUI changeShow={changeShow}></MoreUI>
  )
}
