import { CHANGESHOW, CHANGESHOWSAGA } from './actionTypes'

const changeShow = (show) => {
  return {
    type: CHANGESHOW,
    show
  }
}

const changeShowSaga = (show) => {
  return {
    type: CHANGESHOWSAGA,
    show
  }
}

export {
  changeShow,
  changeShowSaga
}