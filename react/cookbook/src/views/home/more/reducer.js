import { CHANGESHOW } from './actionTypes'
import { Map } from 'immutable'

const defaultState = Map({
  show: true
})

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case CHANGESHOW:
      return state.set('show', action.show)

    default:
      return state
  }
}

export default reducer