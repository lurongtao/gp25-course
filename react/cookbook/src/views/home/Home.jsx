import React, { useState, useCallback, useMemo } from 'react'
import { Route, Switch, Redirect, useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'

import Tabbar from '../../components/tabbar/Tabbar'
import TabItem from '../../components/tabbar/TabItem'

import cookbook from '../../assets/images/cookbook.png'
import cookbookActive from '../../assets/images/cookbook-active.png'
import menu from '../../assets/images/menu.png'
import menuActive from '../../assets/images/menu-active.png'
import more from '../../assets/images/more.png'
import moreActive from '../../assets/images/more-active.png'
import location from '../../assets/images/location.png'
import locationActive from '../../assets/images/location-active.png'

import { Cookbook } from './cookbook'
import Category from './category/Category'
import Map from './map/Map'
import { More } from './more'

export default function Home() {
  const [currentIndex, setCurrentIndex] = useState(0)
  const history = useHistory()
  const show = useSelector(state => state.getIn(['more', 'show']))

  const switchTap = useCallback(
    (index, path) => {
      return () => {
        setCurrentIndex(index)
        history.push(path)
      }
    },
    [history]
  )

  const tabItem = useMemo(() => (
    [<TabItem
      name="cookbook"
      key="cookbook"
      title="美食大全"
      icon={cookbook}
      activeIcon={cookbookActive}
      onClick={switchTap(0, '/cookbook')}
    ></TabItem>,
    <TabItem
      name="category"
      key="cagegory"
      title="分类"
      icon={menu}
      activeIcon={menuActive}
      onClick={switchTap(1, '/category')}
    ></TabItem>,
    <TabItem
      name="map"
      title="地图"
      key="map"
      icon={location}
      activeIcon={locationActive}
      onClick={switchTap(2, '/map')}
    ></TabItem>,
    <TabItem
      name="more"
      title="更多"
      key={more}
      icon={more}
      activeIcon={moreActive}
      onClick={switchTap(3, '/more')}
      test={99}
    ></TabItem>]
  ), [switchTap])

  return (
    <>
      <div style={{height: '100%'}}>
        <Switch>
          <Route path="/cookbook">
            <Cookbook></Cookbook>
          </Route>
          <Route path="/category">
            <Category></Category>
          </Route>
          <Route path="/map">
            <Map></Map>
          </Route>
          <Route path="/more">
            <More></More>
          </Route>
          <Redirect from="/" to="/cookbook"></Redirect>
        </Switch>
      </div>
      <div>
        <Tabbar
          bgcolor="#eee"
          activeColor="#000"
          color="#ccc"
          currentIndex={currentIndex}
        >
          {
            show ? tabItem : tabItem.filter((tab, index) => index !== 2)
          }
        </Tabbar>
      </div>
    </>
  )
}