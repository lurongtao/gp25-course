import { useState, useEffect} from 'react'
import axios from 'axios'

const useLoadData = () => {
  const [list, setList] = useState([])

  useEffect(() => {
    ;(async function(){
      let result = await axios.get('/api/list')
      setList(result.data.data)
    })()
  }, [])

  return {
    list
  }
}

export default useLoadData