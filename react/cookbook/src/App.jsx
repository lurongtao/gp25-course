import React from 'react'
import Home from './views/home/Home'
import './app.css'

export default function App() {
  return (
    <div className="container">
      <Home></Home>
    </div>
  )
}
