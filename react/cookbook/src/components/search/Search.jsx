import React from 'react'
import { SearchWrapper, SearchInput, SearchInputBorder } from './styledSearch'
import { SearchOutline } from 'antd-mobile-icons'
import {
  string,
  bool
} from 'prop-types'

function Search({
  outerBg,
  innerBg,
  hasBorder,
  borderRadius
}) {

  const SearchInputWrapper = hasBorder ? SearchInputBorder : SearchInput 

  return (
    <SearchWrapper
      outerBg={outerBg}
    >
      <SearchInputWrapper
        innerBg={innerBg}
        borderColor="#ee742f"
        borderRadius={borderRadius}
      >
        <SearchOutline color="#ee742f" className="icon"></SearchOutline>
        <span>想吃什么搜这里，如川菜</span>
      </SearchInputWrapper>
    </SearchWrapper>
  )
}

Search.propTypes = {
  outerBg: string,
  innerBg: string,
  hasBorder: bool
}

export default Search