import styled from 'styled-components'
import border from '../styled/border'

const SearchWrapper = styled.div `
  background-color: ${props => props.outerBg};
  padding: .1rem .15rem;
`

const SearchInput = styled.div `
  border-radius: .1rem;
  background-color: ${props => props.innerBg};
  height: .4rem;
  display: flex;
  justify-content: center;
  align-items: center;
  .icon {
    margin-right: .05rem;
  }
`

const SearchInputBorder = border(
  SearchInput
)

export {
  SearchWrapper,
  SearchInput,
  SearchInputBorder
}