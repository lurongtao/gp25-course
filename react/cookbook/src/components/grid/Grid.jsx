import React from 'react'
import GridItem from './GridItem'
import { GridWrapper } from './styledGrid'

function Grid({
  children,
  columns,
  gap
}) {
  return (
    <GridWrapper
      gap={gap}
    >
      {
        React.Children.map(children, child => {
          return (
            React.cloneElement(child, {
              columns,
              gap
            })
          )
        })
      }
    </GridWrapper>
  )
}

Grid.Item = GridItem

export default Grid