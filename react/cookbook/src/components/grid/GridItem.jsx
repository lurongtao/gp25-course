import React from 'react'
import { GridItemWrapper } from './styledGrid'

export default function GridItem({children, columns, gap}) {
  return (
    <GridItemWrapper
      columns={columns}
      gap={gap}
    >
      {children}
    </GridItemWrapper>
  )
}
