import styled from 'styled-components'

function calcWidth(columns) {
  return (100/columns) + '%'
}

function getGapSize(gap) {
  return (gap/100) + 'rem'
}

const GridWrapper = styled.div `
  padding-top: .1rem;
  padding-left: ${props => getGapSize(props.gap)};
  display: flex;
  flex-wrap: wrap;
`

const GridItemWrapper = styled.div `
  width: calc(${props => calcWidth(props.columns)} - ${props => getGapSize(props.gap)});
  margin-right: ${props => getGapSize(props.gap)};
  margin-bottom: ${props => getGapSize(props.gap)};
  display: flex;
  align-items: center;
  justify-content: center;
`

export {
  GridWrapper,
  GridItemWrapper
}