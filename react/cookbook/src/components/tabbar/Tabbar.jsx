import React, { Children, cloneElement } from 'react'

import { TabbarWrapper, TabbarBody } from './styledTabar'

export default function Tabbar({ 
  children, 
  currentIndex,
  bgColor,
  activeColor,
  color
}) {
  return (
    <TabbarWrapper
      bgColor={bgColor}
    >
      <TabbarBody
        borderWidth="1px 0 0 0"
      >
        {
          Children.map(children, (child, index) => {
            return cloneElement(child, {
              index,
              currentIndex,
              activeColor,
              color
            })
          })
        }
      </TabbarBody>
    </TabbarWrapper>
  )
}