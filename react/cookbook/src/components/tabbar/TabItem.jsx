import React, {useCallback} from 'react'

import { TabbarItemWrapper, TabBarItemText } from './styledTabar'

import { useRouteMatch } from 'react-router-dom'

import {
  number,
  string,
  func,
  // oneOf,
  // oneOfType,
  // arrayOf,
  // objectOf,
  // shape,
  // exact
  // element,
  // elementType,
  // instanceOf
} from 'prop-types'
// import PropTypes from 'prop-types'

export default function TabItem({
  title,
  icon,
  activeIcon,
  index,
  currentIndex,
  activeColor,
  color,
  onClick,
  name,
  test
}) {

  const handleClick = useCallback(
    () => {
      onClick()
    },
    [onClick],
  )

  const match = useRouteMatch('/' + name)
  
  return (
    <TabbarItemWrapper
      onClick={handleClick}
    >
      <img src={!!match ? activeIcon : icon } alt="" />
      <TabBarItemText
        color={!!match ? activeColor : color}
      >
        {title}
      </TabBarItemText>
    </TabbarItemWrapper>
  )
}

TabItem.propTypes = {
  title: string,
  icon: string,
  activeIcon: string,
  index: number,
  currentIndex: number,
  activeColor: string,
  color: string,
  onClick: func,
  name: string,
  test: function(props, propName, componentName) {
    if (props.test >= 100) {
      return new Error('传输的数据要小于100')
    }
  }
}