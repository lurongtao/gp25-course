import styled from 'styled-components'

import border from '../../components/styled/border'

const TabbarWrapper = styled.div `
  position: fixed;
  width: 100%;
  bottom: 0;
  height: .5rem;
  background-color: ${props => props.bgColor || '#fff'};
`
const TabbarBody = border(
  styled.div `
    display: flex;
  `
)

const TabbarItemWrapper = styled.div `
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-top: .03rem;
  img {
    width: .25rem;
    height: .25rem;
  }
`

const TabBarItemText = styled.span `
  color: ${ ({color}) => color }
`

export {
  TabbarWrapper,
  TabbarBody,
  TabbarItemWrapper,
  TabBarItemText
}