import styled from 'styled-components'

const border = (Comp) => {
  return styled(Comp) `
    position: relative;
    
    &::after {
      pointer-events: none;
      content: '';
      position: absolute;
      border: solid 1px red;
      border-width: ${ props => props.borderWidth || '1px' };
      border-color: ${ props => props.borderColor || '#ccc' };
      border-style: ${ props => props.borderStyle || 'solid' };
      border-radius: ${ props => props.borderRadius || 0 };
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;

      @media screen and (-webkit-max-device-pixel-ratio:1.49) {
        width: 100%;
        height: 100%;
        transform: scale(1);
      }

      @media screen and (-webkit-min-device-pixel-ratio:1.5) and (-webkit-max-device-pixel-ratio:2.49) {
        width: 200%;
        height: 200%;
        transform: scale(0.5);
      }

      @media screen and (-webkit-min-device-pixel-ratio:2.5) {
        width: 300%;
        height: 300%;
        transform: scale(0.3333333);
      }

      transform-origin: 0 0;
    }
  `
}

export default border