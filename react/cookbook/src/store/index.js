import { createStore, applyMiddleware } from 'redux'
import reducer from './reducer'

import createSagaMiddleware from 'redux-saga'
import sagas from './sagas'

import { Map } from 'immutable'

// Create the saga middleware
const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  reducer,
  Map({}),
  applyMiddleware(sagaMiddleware)
)

// Then run the saga
sagas.map(saga => sagaMiddleware.run(saga))

export default store