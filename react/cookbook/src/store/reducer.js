import {
  combineReducers
} from 'redux-immutable'

import { reducer as cookbook } from '../views/home/cookbook'
import { reducer as more } from '../views/home/more'

export default combineReducers({
  cookbook,
  more
})