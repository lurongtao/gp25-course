import Mock from 'mockjs'

Mock.mock(
  '/api/list',
  {
    "ret": true,
    "data|10-100": [
      {
        "name": "@cword(2,5)",
        "id": function() {
          return Math.random().toString(36).substring(7).split('').join('.')
        },
        "img": "@image('375x250', '@color()', '#FFF', 'png', '!')",
        "all_click": "@float(100, 200, 1, 1)\u4e07",
        "favorites": "@float(1, 3, 1, 1)\u4e07",
      }, 
    ]
  }
)