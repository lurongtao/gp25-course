const defaultState = {
  count: 0
}

const reducer = (state = defaultState, action) => {
  switch(action.type) {
    case 'add':
      return {
        count: state.count + action.num
      }
    default:
      return state
  }
}

module.exports = reducer