exports.loadDataSync = (list) => {
  return {
    type: 'loadData',
    list
  }
}

exports.loadDataAsycn = () => {
  return async (dispatch, getState) => {
    let result = await new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(['a', 'b', 'c'])
      }, 1000)
    })

    dispatch(this.loadDataSync(result))
  }
}