module.exports = ({dispatch, getState}) => {
  return (next) => {
    return (action) => {
      console.log(action)
      next({
        type: 'add',
        num: action + 100
      })
    }
  }
}