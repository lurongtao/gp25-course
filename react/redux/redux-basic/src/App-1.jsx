import React, { useState, useCallback } from 'react'

import { Provider } from './context/size-context'
import CounterProvider from './redux/CounterProvider'

// import Index from './views/enhancer-component/Index.jsx'
// import Index from './views/hooks/Counter'
// import Index from './views/Color'
// import Index from './redux/Counter'
import Index from './views/Counter'

export default function App() {
  const [size, setSize] = useState('50px')

  const handleClick = useCallback(
    () => {
     setSize('100px') 
    },
    []
  )

  return (
    <Provider
      value={{size}}
    >
      <CounterProvider>
        <Index></Index>
      </CounterProvider>
      <div>
        <button onClick={handleClick}>change size</button>
      </div>
    </Provider>
  )
}
