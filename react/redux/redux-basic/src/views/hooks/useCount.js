import { useState, useCallback } from 'react'

const useCount = () => {
  const [count, setCount] = useState(0)

  const add = useCallback(() => {
    setCount((count) => {
      return count + 1
    })
  }, [])

  return {
    count,
    add
  }
}

export default useCount