import { useRef, useEffect } from 'react'

const useUpdate = (fn) => {
  const isLoaded = useRef(false)

  useEffect(() => {
    if(!isLoaded.current) {
      isLoaded.current = true
    } else {
      fn()
    }
  })
}

export default useUpdate