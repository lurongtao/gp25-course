import React, { useEffect, useMemo, useCallback } from 'react'

import useTitle from './useTitle'
import useCount from './useCount'

import useUpdate from './useUpdate'

export default function Counter() {
  // const [isLoaded, setIsLoaded] = useState(false)

  const { count, add } = useCount()
  useTitle(count)

  // 模拟componentDidMount
  useEffect(() => {
    console.log(document.querySelector('#root').innerHTML)

    return () => {
      console.log('unmounted.')
    }
  }, [])

  // 模拟componentDidUpdate
  useUpdate(() => {
    console.log(0)
  })

  const doubleCount = useMemo(() => {
    return count * 2
  }, [count])

  // useEffect(() => {
  //   if (isLoaded) {
  //     console.log(0)
  //   } else {
  //     setIsLoaded(true)
  //   }
  // })
  
  return (
    <div>
      {count} - {doubleCount}
      <button onClick={add}>+</button>
    </div>
  )
}
