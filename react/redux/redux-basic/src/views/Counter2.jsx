import React, { useContext } from 'react'
import { context } from '../redux/store'

export default function Counter2() {
  const { state } = useContext(context)

  return (
    <div>
      {state.count}
    </div>
  )
}
