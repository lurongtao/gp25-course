import React, { useContext } from 'react'
// import { Consumer as ColorConsumer } from '../context/color-context'
// import { Consumer as SizeConsumer } from '../context/size-context'
import { colorContext } from '../context/color-context'
import { sizeContext } from '../context/size-context'

export default function Color() {
  const colorValue = useContext(colorContext)
  const sizeValue = useContext(sizeContext)

  return (
    <div>
      <div
        style={{
          color: colorValue.color,
          fontSize: sizeValue.size
        }}
      >
        hello
      </div>
      {/* <ColorConsumer>
        {
          (colorValue) => {
            return (
              <SizeConsumer>
                {
                  (sizeValue) => {
                    return (
                      <div
                        style={{
                          color: colorValue.color,
                          fontSize: sizeValue.size
                        }}
                      >hello</div>
                    )
                  }
                }
              </SizeConsumer>
            )
          }
        }
      </ColorConsumer> */}
    </div>
  )
}
