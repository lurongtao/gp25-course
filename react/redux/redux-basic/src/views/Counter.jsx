import React, { useContext, useCallback } from 'react'
import { context } from '../redux/store'

import Counter2 from './Counter2'

export default function Counter() {
  const { state, dispatch } = useContext(context)

  const add = useCallback(
    () => {
      dispatch({
        type: 'add'
      })
    },
    [dispatch],
  )

  return (
    <>
      <div>
        {state.count}
        <button onClick={add}>+</button>
      </div>
      <Counter2></Counter2>
    </>
  )
}
