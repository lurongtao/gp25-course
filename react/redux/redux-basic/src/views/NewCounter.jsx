import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

export default function NewCounter() {
  const count = useSelector(state => state.count)
  const dispatch = useDispatch()

  return (
    <div>
      {count} <button onClick={() => dispatch({type: 'add'})}>+</button>
    </div>
  )
}
