import React from 'react'
import Header from './Header'
import Footer from './Footer'
import Body from './Body'

const Index = () => {
  const _render = () => {
    return (
      <h1>标题</h1>
    )
  }

  return (
    <>
      <Header
        render={_render}
      ></Header>
      <Body msg="hi"></Body>
      <Footer>
        {
          () => {
            return (
              <div>footer content</div>
            )
          }
        }
      </Footer>
    </>
  )
}

export default Index