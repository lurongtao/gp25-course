import React from 'react'
import bodyHoc from '../../hoc/bodyHoc'

function Body({title, msg}) {
  return (
    <div>
      {title} - {msg}
    </div>
  )
}

export default bodyHoc(Body)