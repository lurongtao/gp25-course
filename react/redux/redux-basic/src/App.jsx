import React from 'react'
import { Provider } from 'react-redux'
import Index from './views/NewCounter'
import store from './store'

export default function App() {
  return (
    <Provider
      store={store}
    >
      <Index></Index>
    </Provider>
  )
}
