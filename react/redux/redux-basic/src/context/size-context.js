import { createContext } from 'react'

const sizeContext = createContext()

const { Provider, Consumer } = sizeContext

export {
  sizeContext,
  Provider,
  Consumer
}