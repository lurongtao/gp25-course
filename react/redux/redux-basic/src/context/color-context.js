import { createContext } from 'react'

const colorContext = createContext({
  color: 'red'
})

const { Provider, Consumer } = colorContext

export {
  colorContext,
  Provider,
  Consumer
}