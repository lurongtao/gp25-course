import React from 'react'

import ReactDOM from 'react-dom'

import App from './App.jsx'

ReactDOM.render(
  <App></App>,
  document.querySelector('#root')
)

setTimeout(() => {
  // ReactDOM.unmountComponentAtNode(document.querySelector('#root'))
}, 3000)