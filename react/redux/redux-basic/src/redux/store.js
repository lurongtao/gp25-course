import { createContext } from 'react'

export const defaultState = {
  count: 0
}

export const reducer = (state, action) => {
  switch(action.type) {
    case 'add':
      return {
        count: state.count + 1
      }

    case 'minus':
      return {
        count: state.count - 1
      }

    default:
      return state
  }
}

export const context = createContext()