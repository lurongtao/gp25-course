import React, { useReducer } from 'react'
import { defaultState, reducer, context } from "./store"

const CounterProvider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, defaultState)

  return (
    <context.Provider
      value={{
        state,
        dispatch
      }}
    >
      {children}
    </context.Provider>
  )
}

export default CounterProvider