const Koa = require('koa')
const path = require('path')
const koa = new Koa()
const router = require('./routes/weixin')
const bodyParser = require('koa-bodyparser')
const views = require('koa-views')
const static = require('koa-static')
const cors = require('@koa/cors')

// 静态资源目录对于相对入口文件index.js的路径
const staticPath = './public'

koa.use(static(
  path.join( __dirname,  staticPath)
))

// 解析body
koa.use(bodyParser())

// 加载模板引擎
koa.use(views(path.join(__dirname, './views'), {
  extension: 'ejs'
}))

koa.use(cors())

// 定义路由
koa.use(router.routes()).use(router.allowedMethods())

// 端口监听
koa.listen(3333, () => {
  console.log('localhost:3333')
})