const Router = require('@koa/router')
const { 
  auth,
  autoreply,
  sign
} = require('../controllers/weixin')

const router = new Router()

router.get('/', auth)
router.post('/', autoreply)

router.get('/sign', sign)

module.exports = router