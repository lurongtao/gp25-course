// 使用 Mock
var Mock = require('mockjs')
var data = Mock.mock({
    // 属性 list 的值是一个数组，其中含有 1 到 10 个元素
    'list|1-10': [{
      // 属性 id 是一个自增数，起始值为 1，每次增 1
      'id|+10': 100,
      'name|5': 'abc',
      'age|+1': 2,
      'inventory|50-100': 1,
      'price|200-300.2-3': 10,
      'saleout|1-3': true,
      'city|1-2': {
        'name': 'bj',
        'location': 200
      },
      'hobies|1-2': ['football', 'baseball', 'basketball'],
      'list': function() {
        return this.hobies
      },
      'regexp1': /[a-z][A-Z][0-9]/,
      'regexp2': /\w\W\s\S\d\D/,
      'regexp3': /\d{5,10}/,
      'title': '@cword(1,3)',
      'createtime': '@date(yyyy年MM月dd日)',
      'img': "@image('200x100', '#ffcc33', '#FFF', 'png', '')"
    }],
})
// 输出结果
console.log(JSON.stringify(data, null, 4))